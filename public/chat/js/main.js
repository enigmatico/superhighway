const socket = new WebSocket('wss://' + window.location.hostname + ':8080');
var userdata, userinput, chatlog;

function sendChat(line){
    if(userdata){
        sendMessage("CHAT", {
            user: userdata.unique_id,
            hub: "reserved",
            media: "reserved",
            message: line
        });
        /*socket.send(JSON.stringify({
            command: "CHAT",
            params: [userdata.unique_id, "prototype", line]
        }));*/
    }
}

function createResponse(type_, data_){
    return {
      type: type_,
      data: data_
    }
}

function sendMessage(type_, data_){
    console.log("Sending message: ", type_, data_);
    socket.send(JSON.stringify(createResponse(type_, data_)));
}

function getSessionValue(cookiename){
    let cookiel = document.cookie.match(new RegExp(cookiename + "=([^\s]+)"))
    if(cookiel.length < 2)
        return undefined;
    else
        return cookiel[1];
}

function MessageToHtml(author, date, line, media=undefined){
    let msg = "";
    msg += "<div class=\"chat-entry\">\n";
        msg += "<div class=\"chat-entry-container flexi-container\">\n";
            msg += "<div class=\"chat-entry-avatar-place flexi-log-avatar\">\n";
                msg += "<img src=\"STATIC/face.png\" class=\"chat-entry-avatar\">\n";
            msg += "</div>\n";
            msg += "<div class=\"message-log-place flexi-log-data\">\n";
                msg += "<div class=\"chat-entry-info-container\">\n";
                    msg += "<span class=\"chat-entry-username\">"+author.username+"</span>\n";
                    msg += "<span class=\"chat-entry-date\">"+ date + "</span>\n";
                msg += "</div>\n";
                msg += "<p class=\"chat-entry-line\">" + line + "</p>\n";
            msg += "</div>\n";
        msg += "</div>\n";
    msg += "</div>\n";
    return msg;
}

window.addEventListener('load', function(e){
    userinput = document.getElementById("userinput-textbox");
    chatlog = document.getElementById("channel-chatlog");

    userinput.addEventListener('keydown', function(e){
        if(e.key == "Enter"){
            sendChat(userinput.value);
            userinput.value = "";
        }
    });

    sendMessage("HELLO", {});

    socket.onmessage = function(message){
        console.log("I GOT SOMETHING: ", message);
        let msg = JSON.parse(message.data);
        switch(msg.type){
            case "HELLO":
                let c = getSessionValue(msg.data.cookiename);
                if(c){
                    sendMessage("AUTHORIZE", {session_id: c});
                }else{
                    // Not authorized
                    console.log("You are not logged in...");
                }
            break;
            case "ACCOUNT":
                userdata = msg.data.account;
                console.log("Welcome, ", userdata.username);
            break;
            case "ERROR":
                console.log("Error: ", msg.status, msg.description);
            break;
            case "CHAT":
                if(chatlog.scrollTop >= (chatlog.scrollHeight - chatlog.clientHeight)-5){
                    chatlog.innerHTML += MessageToHtml(msg.data.from, msg.data.date, msg.data.message);
                    chatlog.scrollTop = (chatlog.scrollHeight - chatlog.clientHeight);
                }else{
                    chatlog.innerHTML += MessageToHtml(msg.data.from, msg.data.date, msg.data.message);
                }
            break;
            default:
                console.log("I don't know what this is...");
        }


        /*if(data.response !== undefined){
            switch(data.subject){
                case "HI":
                    cookiel = document.cookie.match(new RegExp(data.data.cookiename + "=([^\s]+)"))
                    if(cookiel.length < 2)
                        //Not autorized
                        break;
                    console.log("Cookie: ", cookiel[1]);
                    socket.send(JSON.stringify({
                        command: "SESAUTH",
                        params: [cookiel[1]]
                    }));
                break;
                case "SESAUTH":
                    if(data.response < 1){
                        //Not authorized
                    }else{
                        //Got authorization!
                        userdata = data.data;
                        console.log("Username: " + userdata.username);
                    }
                break;
            }
        }else if(data.command !== undefined){
            switch(data.command){
                case "CHAT":
                    if(chatlog.scrollTop >= (chatlog.scrollHeight - chatlog.clientHeight)-5){
                        chatlog.innerHTML += data.data;
                        chatlog.scrollTop = (chatlog.scrollHeight - chatlog.clientHeight);
                    }else{
                        chatlog.innerHTML += data.data;
                    }
                break;
            }
        }else{
            //Wut
        }*/
    }
});
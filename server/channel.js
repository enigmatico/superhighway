const dbman = require('./dbman.js');

class Channel {
    constructor(data){
        for(const[key, value] of Object.entries(Channel.model)) {
            if(key === "" || value === "" || key === "db_table_info") continue;
            this[key] = data[key];
        }
    }

    static SetModel(){
        return new Promise((resolve, reject)=>{
            this.model = {
                id: "bigint(20) NOT NULL AUTO_INCREMENT",
                name: "varchar(30) NOT NULL",
                description: "varchar(60)",
                hub: "bigint(20) NOT NULL",
                type: "tinyint DEFAULT 0",
                channel_index: "int DEFAULT 0",
                db_table_info: {
                    name: "shw_channels",
                    charset: "utf8mb4",
                    primary: "id",
                    constraints: {
                        hub: "shw_hub(id)"
                    }
                }
            };
            this.numfields = this.model.length;
            try{
                dbman.SyncModelP(this.model).catch((err)=>{throw err}).then((res) => resolve());
            }catch(Error){
                reject(Error);
            }
        });
    }

    static GetP(id, session=false){
        let idfield = '';
        if(typeof(id) == 'number'){
            idfield = 'id';
        }else if(typeof(id) == 'string'){
            if(session===false)
                idfield = 'mail';
            else
                idfield = 'session';
        }
        return new Promise((resolve,reject) => {
            dbman.GetEntityP(this.model, id, idfield)
            .catch((err)=>reject(err))
            .then((res)=>{
                resolve(new Channel(res[0]));
            });
        });
    }

    static CreateP(data, callback){
        let mdata = {};
        let c = 0;
        for(const[key, value] of Object.entries(this.model))
        {
            if(c > data.length){
                mdata[key] = null;
            }else{
                mdata[key] = data[c];
            }
            c++;
        }
        let newuser = new Channel(mdata);
        return new Promise((resolve, reject)=>{
            newuser.SyncP().catch((err)=>reject(err)).then((entity)=>resolve(entity));
        });
        
    }

    DeleteP(){
        return new Promise((resolve, reject) => {
            dbman.RemoveEntityP(Channel.model, this)
            .catch((err)=>{reject(err)})
            .then(()=>resolve());
        });
        
    }

    SyncP() {
        return new Promise((resolve, reject) => {
            dbman.SyncEntityP(Channel.model, this)
            .catch((err)=>reject(err))
            .then((entity)=>resolve(entity));
        });
        
    }
}

module.exports = Channel;
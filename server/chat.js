const fs = require('fs');
const WebSocket = require('ws');
const WebSocketServer = WebSocket.Server;
const Log = require('../utils.js').Loging;
const user = require('./user.js');

var hub_users = [];

class Chat {
    static Initialize(webServer){
        this.wss = new WebSocketServer({server: webServer});

        this.wss.on('connection', (ws) => {
            ws.on('message', (message) => {
              ws.resp = {};
              console.log('received: %s', message);
              let msg = JSON.parse(message);

              switch(msg.type){
                case "HELLO":
                  ws.resp = this.createResponse("HELLO",{cookiename: "superhighway_session"});
                  ws.send(JSON.stringify(ws.resp));
                break;
                case "AUTHORIZE":
                  console.log("Auth request: " + msg.data.session_id);
                  if(ws.clientSession === undefined){
                    user.GetP(msg.data.session_id, true)
                    .then((user) => {
                      ws.clientSession = msg.data.session_id;
                      ws.user = user;
                      ws.resp = this.createResponse("ACCOUNT", {account: ws.user.getInfo()});
                      ws.send(JSON.stringify(ws.resp));
                      hub_users.push(ws);
                      console.log("Assigned session: " + ws.clientSession);
                    })
                    .catch((err)=>{
                      Log.Error(err);
                      ws.resp = this.createError(1, "Not authorized.");
                      ws.send(JSON.stringify(ws.resp));
                    });
                  }
                break;
                case "CHAT":
                  hub_users.forEach((wsx)=>{
                    wsx.send(JSON.stringify(this.createResponse("CHAT", {
                      from: ws.user.getPublicInfo(),
                      date: new Date().toUTCString(),
                      media: "reserved",
                      message: msg.data.message
                    })));
                  });
                break;
                default:
                  ws.resp = this.createError(0, "The server could not understand the message.");
                  ws.send(JSON.stringify(ws.resp));
              }

            });
          });
          
          this.wss.broadcast = function(data) {
            this.clients.forEach(function(client) {
              if(client.readyState === WebSocket.OPEN) {
                client.send(data);
              }
            });
          };

        Log.debug("Started web socket",false,true);
    }

    static createError(status_, desc){
            return {
              type: "ERROR",
              status: status_,
              description: desc
            }
    }

    static createResponse(type_, data_){
      return {
        type: type_,
        data: data_
      }
    }
}

module.exports = Chat;
/* Module requirement */
const mysql = require('mysql');
//const util = require('util');
const Log = require('../utils.js').Loging;

class dbman {

    static Initialize(dbdata, callbackfun)
    {
        this.config = dbdata;
        this.pool = mysql.createPool({
            host:       dbdata.server,
            user:       dbdata.user,
            password:   dbdata.password,
            database:   dbdata.database,
            acquireTimeout: dbdata.pooling.acquireTimeout,
            waitForConnections: dbdata.pooling.waitForConnections,
            connectionLimit: dbdata.pooling.connectionLimit,
            queueLimit: dbdata.pooling.queueLimit
        });

        this.tableprefix = dbdata.tableprefix;
        callbackfun();
    }

    static GetConnectionPool(){
        return this.pool;
    }

    static Escape(str){
        return mysql.escape(str);
    }

    static QueryP(querystr) {
        return new Promise((resolve, reject) => {
            this.pool.query(querystr, (error, results, fields) => {
                if(error)
                    reject(error);
                resolve([results,fields]);
            });
        });
    }

    static QueryP(querystr, args) {
        //console.log("\n -- DEV SQL: \n" + querystr + "\n -- ARGS: ", args);
        return new Promise((resolve, reject) => {
            this.pool.query(querystr, args, (error, results, fields) => {
                if(error)
                    reject(error);
                resolve([results,fields]);
            });
        });
    }

    static Query(querystr, callbackfun) {
        this.pool.query(querystr, function(errormsg, results, fields){
            callbackfun(errormsg, results, fields);
        });
    }

    static Query(querystr, queryvals, callbackfun) {
        this.pool.query(querystr, queryvals, function(errormsg, results, fields){
            callbackfun(errormsg, results, fields);
        });
    }

    static CheckTableP(tablename){
        return new Promise((resolve, reject) => {
            dbman.QueryP({
                sql: "SELECT * FROM information_schema.tables WHERE table_schema = ? AND table_name = ?",
                values: [this.config.database, tablename]
            })
            .then((result) => {
                if(result[0].length > 0)
                    resolve(true);
                else
                    resolve(false);
            })
            .catch((err) => reject(err));
        });
    }

    static CheckTable(tablename, callbackfun){
        this.pool.query({
            sql: "SELECT * FROM information_schema.tables WHERE table_schema = ? AND table_name = ?",
            values: [this.config.database, tablename]
        }, function(error, result, fields){
            if(error) throw error;
            if(result.length > 0)
                callbackfun(true);
            else
                callbackfun(false);
        });
    }

    static SyncModelP(model){
        return new Promise((resolve, reject)=>{
            let modified = false;
            dbman.CheckTableP(model.db_table_info.name)
            .then((r)=>{
                if(r === true){
                    //Update table
                    dbman.QueryP("SELECT * FROM " + model.db_table_info.name)
                    .then((res) => {
                        let before = '';
                        for(var field of res[1]){
                            let intable = false;
                            for(const[key, value] of Object.entries(model)){
                                if(key === "db_table_info"){
                                    continue;
                                }
                                if(key === field.name){
                                    intable = true;
                                    break;
                                }
                            }
                            if(intable === false){
                                /* Yes these go Asynchronous */
                                dbman.Query({
                                    sql: "ALTER TABLE " + model.db_table_info.name + " DROP " + field.name
                                }, function(err, result, fields2){
                                    if(err) throw err;
                                });
                                modified = true;
                            }
                        }
    
                        for(const[key, value] of Object.entries(model)){
                            if(key === "db_table_info") continue;
                            let intable = false;
                            for(var field of res[1])
                            {
                                if(field.name === key)
                                    intable = true;
                            }
                            if(!intable){
                                /* Yes these go Asynchronous */
                                dbman.Query({
                                    sql: "ALTER TABLE " + model.db_table_info.name + " ADD " + key + " " + value + before
                                }, function(err, result, fields){
                                    if(err) throw err;
                                });
                                modified = true;
                            }
                            before = ' AFTER ' + key;
                        }
                        if(modified)
                            Log.debug("WARNING: Table modified: " + model.db_table_info.name);
                        resolve();
                    })
                    .catch((err) => {reject(err)});

                }else{
                    // New table
                    let sql_ = "CREATE TABLE " + model.db_table_info.name + " (\n";
                    for(const[key, value] of Object.entries(model)){
                        if(key === "db_table_info" || key === "" || value === "") continue;
                        sql_ += key + " " + value + ",\n";
                    }
                    if(model.db_table_info.primary !== undefined)
                        sql_ += "PRIMARY KEY (" + model.db_table_info.primary + ")";
                    if(model.db_table_info.constraints !== undefined){
                        sql_ += ",";
                        for(const[key, value] of Object.entries(model.db_table_info.constraints)){
                            sql_ += "\nFOREIGN KEY (" + key + ") REFERENCES " + value + " ON DELETE CASCADE,";
                        }
                        sql_ = sql_.slice(0, -1);
                    }
                    if(model.db_table_info.unique !== undefined){
                        sql_ += ",\nUNIQUE(" + model.db_table_info.unique + ")";
                    }
        
                    sql_ += "\n) DEFAULT CHARSET=" + model.db_table_info.charset;
        
                    //console.log("\n--DEV SQL: " + sql_ + "\n");

                    dbman.QueryP(sql_).then((res)=>{
                        Log.debug("IMPORTANT: Created a new table: " + model.db_table_info.name);
                        resolve(res[0]);
                    })
                    .catch((err) => reject(err));
                }
            })
            .catch((err) => reject(err));
        });
    }

    static SyncModel(model){
        dbman.CheckTable(model.db_table_info.name, function(s){
            if(s === false){
                let sql_ = "CREATE TABLE " + model.db_table_info.name + " (\n";
                for(const[key, value] of Object.entries(model)){
                    if(key === "db_table_info" || key === "" || value === "") continue;
                    sql_ += key + " " + value + ",\n";
                }
                if(model.db_table_info.primary !== undefined)
                    sql_ += "PRIMARY KEY (" + model.db_table_info.primary + ")";
                if(model.db_table_info.constraints !== undefined){
                    sql_ += ",";
                    for(const[key, value] of Object.entries(model.db_table_info.constraints)){
                        sql_ += "\nFOREIGN KEY (" + key + ") REFERENCES " + value + " ON DELETE CASCADE,";
                    }
                    sql_ = sql_.slice(0, -1);
                }

                sql_ += "\n) DEFAULT CHARSET=" + model.db_table_info.charset;

                dbman.Query(sql_,function(err,result,fields){
                    if(err) throw err;
                });
            }else{
                dbman.Query("SELECT * FROM " + model.db_table_info.name, function(err,result,fields){
                    if(err) throw err;
                    let before = '';

                    for(var field of fields){
                        let intable = false;
                        for(const[key, value] of Object.entries(model)){
                            if(key === "db_table_info"){
                                continue;
                            }
                            if(key === field.name){
                                intable = true;
                                break;
                            }
                        }
                        if(intable === false){
                            dbman.Query({
                                sql: "ALTER TABLE " + model.db_table_info.name + " DROP " + field.name
                            }, function(err, result, fields2){
                                if(err) throw err;
                            });  
                        }
                    }

                    for(const[key, value] of Object.entries(model)){
                        if(key === "db_table_info") continue;
                        let intable = false;
                        for(var field of fields)
                        {
                            if(field.name === key)
                                intable = true;
                        }
                        if(!intable){
                            dbman.Query({
                                sql: "ALTER TABLE " + model.db_table_info.name + " ADD " + key + " " + value + before
                            }, function(err, result, fields){
                                if(err) throw err;
                            });
                        }
                        before = ' AFTER ' + key;
                    }
                });
            }
        });
    }

    static GetEntity(model, id, callback, byfield = undefined){
        let query = "";
        if(!byfield)
            query = "SELECT * FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?";
        else
            query = "SELECT * FROM " + model.db_table_info.name + " WHERE " + byfield + " = ?";
        dbman.Query({
            sql: query,
            values: [id]
        },function(err,res,fields){
            if(err) throw err;
            if(res.length < 1)
                callback("Error: Entity not found.");
            else
                callback(undefined, res);
        });
    }

    static GetEntityP(model, id, byfield = undefined){
        let query = "";
        if(!byfield)
            query = "SELECT * FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?";
        else
            query = "SELECT * FROM " + model.db_table_info.name + " WHERE " + byfield + " = ?";
        return new Promise((resolve, reject)=>{
            dbman.QueryP({
                sql: query,
                values: [id]
            })
            .catch((err)=>reject(err))
            .then((res)=>{
                if(res[0].length < 1)
                    reject("Entity not found:", id);
                else{
                    resolve(res[0]);
                }
            });
        });
    }

    static SyncEntityP(model, entity){
        let sql_ = "";
        let val_ = [];
        return new Promise((resolve, reject) => {
            dbman.QueryP({
                sql: "SELECT * FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?",
                values: [entity[model.db_table_info.primary]]
            })
            .catch((err) => reject(err))
            .then((res)=>{
                if(res[0].length > 0){
                    // Update
                    sql_ = "UPDATE "+ model.db_table_info.name + " SET ";
                    for(const[key, value] of Object.entries(model)){
                        if(key === model.db_table_info.primary || key === "db_table_info") continue;
                        sql_ += key + " = ?,";
                        val_.push((entity[key]===undefined)?null:entity[key]);
                    }
                    sql_ = sql_.slice(0, -1);
                    sql_ += " WHERE " + model.db_table_info.primary + " = " + entity[model.db_table_info.primary];
                }else{
                    //Insert
                    sql_ = "INSERT INTO " + model.db_table_info.name + " (";
                    for(const[key, value] of Object.entries(model)){
                        if(key === model.db_table_info.primary || key === "db_table_info") continue;
                        sql_ += key + ",";
                        val_.push((entity[key]===undefined)?null:entity[key]);
                    }
                    sql_ = sql_.slice(0, -1);
                    sql_ +=  ") VALUES (";
                    for(var x = 0; x < val_.length; x++)
                    {
                        sql_ += ((x === val_.length - 1)?"?":"?,");
                    }
                    sql_ += ")";
                }
            })
            .then(() => dbman.QueryP(sql_, val_))
            .catch((err) => reject(err))
            .then((res) => {
                if(entity[model.db_table_info.primary] !== res[0].insertId)
                entity[model.db_table_info.primary] = res[0].insertId;
                if(res[0].affectedRows < 1 || res[0].length > 1){
                        reject("Error: could not sync entity.");
                }else{
                        resolve(entity);
                }
            })
            .catch((err) => {throw err});
        });
    }

    static SyncEntity(model, entity, callback){
        let sql_ = "";
        let val_ = [];
        dbman.Query({
            sql: "SELECT * FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?",
            values: [entity[model.db_table_info.primary]]
        },function(err, res, fields){
            if(err) throw err;
            if(res.length > 0)
            {
                // Update
                sql_ = "UPDATE "+ model.db_table_info.name + " SET ";
                for(const[key, value] of Object.entries(model)){
                    if(key === model.db_table_info.primary || key === "db_table_info") continue;
                    sql_ += key + " = ?,";
                    val_.push((entity[key]===undefined)?null:entity[key]);
                }
                sql_ = sql_.slice(0, -1);
                sql_ += " WHERE " + model.db_table_info.primary + " = " + entity[model.db_table_info.primary];
            }else{
                //Insert
                sql_ = "INSERT INTO " + model.db_table_info.name + " (";
                for(const[key, value] of Object.entries(model)){
                    if(key === model.db_table_info.primary || key === "db_table_info") continue;
                    sql_ += key + ",";
                    val_.push((entity[key]===undefined)?null:entity[key]);
                }
                sql_ = sql_.slice(0, -1);
                sql_ +=  ") VALUES (";
                for(var x = 0; x < val_.length; x++)
                {
                    sql_ += ((x === val_.length - 1)?"?":"?,");
                    //val_.push((entity[Object.entries(model)[x][0]]===undefined)?null:entity[Object.entries(model)[x][0]]);
                }
                sql_ += ")";
            }
            dbman.Query({
                sql: sql_,
                values: val_
            },function(err2, res2, fields2){
                if(err2) throw err2;
                if(entity[model.db_table_info.primary] !== res2.insertId)
                    entity[model.db_table_info.primary] = res2.insertId;
                if(res2.affectedRows < 1 || res2.length > 1){
                    if(callback !== undefined)
                        callback("Error: could not sync entity.", entity);
                }else{
                    if(callback !== undefined)
                        callback(undefined, entity);
                }
            });
        });
    }

    static RemoveEntityP(model, entity){
        return new Promise((resolve, reject)=>{
            dbman.QueryP({
                sql: "DELETE FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?",
                values: [entity[model.db_table_info.primary]]
            })
            .then((res)=>{
                if(res.affectedRows < 1){
                    if(callback !== undefined)
                        reject("Error: could not remove entity.");
                }else{
                    if(callback !== undefined)
                        resolve();
                }
            })
            .catch((err)=>reject(err));
        });
    }

    /* TODO: Untested!! */
    static RemoveEntity(model, entity, callback){
        dbman.Query({
            sql: "DELETE FROM " + model.db_table_info.name + " WHERE " + model.db_table_info.primary + " = ?",
            values: [entity[model.db_table_info.primary]]
        },function(err,res,fields){
            if(err) throw err;
            if(res.affectedRows < 1){
                if(callback !== undefined)
                    callback("Error: could not remove entity.", self);
            }else{
                if(callback !== undefined)
                    callback(undefined, entity);
            }
        });
    }

    static Close() {
        this.pool.end(function(err){
            if(err) throw err + new Error().stack;
        });
    }

    static CloseP() {
        return new Promise((resolve, reject) => {
            this.pool.end((err) => {
                if(err) reject(err);
                resolve();
            });
        });
    }
}

module.exports = dbman;
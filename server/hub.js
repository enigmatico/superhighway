const dbman = require('./dbman.js');
const Log = require('../utils.js').Loging;
const channel = require('./channel.js');

class Hub {
    constructor(data){
        for(const[key, value] of Object.entries(Hub.model)) {
            if(key === "" || value === "" || key === "db_table_info") continue;
            this[key] = data[key];
        }
    }

    static SetModel(){
        return new Promise((resolve, reject)=>{
            this.model = {
                id: "bigint(20) NOT NULL AUTO_INCREMENT",
                name: "varchar(30) NOT NULL",
                description: "varchar(256)",
                userlimit: "int DEFAULT 1000",
                creation_date: "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
                db_table_info: {
                    name: "shw_hub",
                    charset: "utf8mb4",
                    primary: "id"
                }
            };
            this.numfields = this.model.length - 1;
            try{
                dbman.SyncModelP(this.model).catch((err)=>{throw err}).then((res) => resolve());
            }catch(Error){
                reject(Error);
            }
        });
    }

    static GetP(id, session=false){
        let idfield = '';
        if(typeof(id) == 'number'){
            idfield = 'id';
        }else if(typeof(id) == 'string'){
            if(session===false)
                idfield = 'mail';
            else
                idfield = 'session';
        }
        return new Promise((resolve,reject) => {
            dbman.GetEntityP(this.model, id, idfield)
            .catch((err)=>reject(err))
            .then((res)=>{
                resolve(new Hub(res[0]));
            });
        });
    }

    static CreateP(data){
        let mdata = {};
        let c = 0;
        for(const[key, value] of Object.entries(this.model))
        {
            if(c > data.length){
                mdata[key] = null;
            }else{
                mdata[key] = data[c];
            }
            c++;
        }
        let newuser = new Hub(mdata);
        return new Promise((resolve, reject)=>{
            newuser.SyncP().catch((err)=>reject(err)).then((entity)=>resolve(entity));
        });
        
    }

    static NewHubP(data, channels) {
        return new Promise((resolve, reject) => {
            Hub.CreateP(data)
            .then((myhub)=>{
                for(const[key, value] of Object.entries(channels))
                {
                    let ctype, pos = 0;
                    switch(value){
                        case "cathegory":
                            ctype = 2;
                        break;
                        case "voice":
                            ctype = 1;
                        break;
                        case "text":
                        default:
                            ctype = 0;
                    }
                    channel.CreateP([0,key,null,myhub.id,ctype, pos])
                    .then((mychannel) => myhub.PushChannel(mychannel))
                    .catch((err)=>{Log.Error("Error creating channel " + key); reject(err)});
                }
                resolve(myhub);
            })
            .catch((err)=>{reject(err)});
        });
    }

    PushChannel(channel) {
        if(this.channels === undefined)
            this.channels = [];
        this.channels.push(channel);
    }

    /* Untested */
    RemoveChannel(id){
        for(var x = 0; x < this.channels.length; x++){
            if(this.channels[x] === id){
                this.channels.splice(x,1);
                return;
            }
        }
    }

    DeleteP(){
        return new Promise((resolve, reject) => {
            dbman.RemoveEntityP(Hub.model, this)
            .catch((err)=>{reject(err)})
            .then(()=>resolve());
        });
        
    }

    SyncP() {
        return new Promise((resolve, reject) => {
            dbman.SyncEntityP(Hub.model, this)
            .catch((err)=>reject(err))
            .then((entity)=>resolve(entity));
        });
        
    }
}

module.exports = Hub;
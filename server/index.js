/* Module requirement */
const path = require('path');
const fs = require('fs');
const dbman = require('./dbman.js');
const WebServer = require('./webserver.js');
const Chat = require('./chat.js');
const Log = require('../utils.js').Loging;

/* Object classes */
const user = require('./user.js');
const hub = require('./hub.js');
const channel = require('./channel.js');

/* Global variables */
var debugMode = false;
var curdate = new Date();

/* Global constants */
const main_dir = path.resolve('.');
const server_dir = main_dir + '/server';
const log_dir = server_dir + '/logs';
const cert_dir = server_dir + '/ssl';
const public_dir = main_dir + '/public';
const log_file = log_dir + '/log_' + curdate.getMonth() + '_' + curdate.getDay() + '_' + curdate.getFullYear() + '.log';
const pArgs = process.argv.slice(2);

/* JSON requirements */
const sconfig = require(main_dir + '/serverconfig.json');

/* Server configuration */
const webconfig = {
    bind: sconfig["web_options"]["bind_to"],
    port: sconfig["web_options"]["port"]
};

/* SSL certificate configuration */
const sslconfig = {
    key: fs.readFileSync(cert_dir + '/' + sconfig["ssl_options"]["ssl_key"]),
    cert: fs.readFileSync(cert_dir + '/' + sconfig["ssl_options"]["ssl_cert"])
};

/* MySQL configuration */
const sqlconfig = {
    server: sconfig["mysql_options"]["mysql_server"],
    user: sconfig["mysql_options"]["mysql_user"],
    password: sconfig["mysql_options"]["mysql_password"],
    database: sconfig["mysql_options"]["mysql_database"],
    tableprefix: sconfig["mysql_options"]["mysql_prefix"],
    pooling: {
        acquireTimeout: sconfig["mysql_options"]["pooling"]["acquireTimeout"],
        waitForConnections: sconfig["mysql_options"]["pooling"]["waitForConnections"],
        connectionLimit: sconfig["mysql_options"]["pooling"]["connectionLimit"],
        queueLimit: sconfig["mysql_options"]["pooling"]["queueLimit"]
    }
};

/* Default Hub */
const defaulthub = {
    enabled: sconfig["default_hub"]["enabled"],
    name: sconfig["default_hub"]["name"],
    userlimit: sconfig["default_hub"]["user_limit"],
    description: sconfig["default_hub"]["description"],
    channels: sconfig["default_hub"]["channels"]
}

/* Argument parsing */
for(var x = 0; x < pArgs.length; x++) {
    switch(pArgs[x])
    {
        case '--debug':
            debugMode = true;
        break;
    }
}

/* Program initialization */
Log.Initialize(log_dir, log_file, debugMode);

/* Database initialization */
dbman.Initialize(sqlconfig, () => {

    /* Launch a test query to test the connection. */
    dbman.QueryP("SHOW TABLES")
    .catch((err) => {
        Log.Error("Could not connect to the database.\n Check your configuration in the serverconfig.json file and make sure your MySQL server is up and running.\n" + err);
        Log.Close();
        process.exit();
    });

    /* Initialize entity models */
    hub.SetModel()
    .then(() => user.SetModel())
    .then(() => hub.SetModel())
    .then(() => channel.SetModel())
    /* Check for default hub */
    .then(() => dbman.QueryP("SELECT * FROM shw_hub WHERE name = '" + defaulthub.name + "'"))
    .then((res) => {
        if(res[0].length < 1 && defaulthub.enabled){
            // Create default hub
            Log.debug("Creating default hub...", false, true);
            hub.NewHubP([0, defaulthub.name, defaulthub.description, defaulthub.userlimit, null], defaulthub.channels)
            .then((myhub) => Log.debug("Hub " + myhub.name + " created with ID " + myhub.id))
            .catch((err)=>{Log.Error("Could not create default hub " + defaulthub.name); throw err});
        }
    })
    /* Initialize the HTTPS server and websocket */
    .then(() => WebServer.initServer(webconfig, sslconfig, public_dir))
    .then(() => Chat.Initialize(WebServer.server))
    .catch((err) => {Log.Error(err.toString()); throw err});
});

/* Program Finalize */
function Finalize(){
    Log.debug("Server shutdown", false, true);
    dbman.CloseP()
    .then(() => Log.Close())
    .then(()=>process.exit())
    .catch((err)=>{throw err});
}

/* Handle SIGINT interrupt (Ctrl+C) */
if (process.platform === "win32") {
    var rl = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout
    });
  
    rl.on("SIGINT", function () {
      process.emit("SIGINT");
    });
  }
  
  process.on("SIGINT", function () {
    Finalize();
  });
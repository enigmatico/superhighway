const dbman = require('./dbman.js');

class Foo {
    constructor(data){
        for(const[key, value] of Object.entries(Foo.model)) {
            if(key === "" || value === "" || key === "db_table_info") continue;
            this[key] = data[key];
        }
    }

    static SetModel(){
        return new Promise((resolve, reject)=>{
            this.model = {
                id: "bigint(20) NOT NULL AUTO_INCREMENT",
                foo: "varchar(30) NOT NULL",
                db_table_info: {
                    name: "tablename",
                    charset: "utf8mb4",
                    primary: "id"
                }
            };
            this.numfields = this.model.length;
            try{
                dbman.SyncModelP(this.model).catch((err)=>{throw err}).then((res) => resolve());
            }catch(Error){
                reject(Error);
            }
        });
    }

    static GetP(id, session=false){
        let idfield = '';
        if(typeof(id) == 'number'){
            idfield = 'id';
        }else if(typeof(id) == 'string'){
            if(session===false)
                idfield = 'mail';
            else
                idfield = 'session';
        }
        return new Promise((resolve,reject) => {
            dbman.GetEntityP(this.model, id, idfield)
            .catch((err)=>reject(err))
            .then((res)=>{
                resolve(new Foo(res[0]));
            });
        });
    }

    static CreateP(data, callback){
        let mdata = {};
        let c = 0;
        for(const[key, value] of Object.entries(this.model))
        {
            if(c > data.length){
                mdata[key] = null;
            }else{
                mdata[key] = data[c];
            }
            c++;
        }
        let newuser = new Foo(mdata);
        return new Promise((resolve, reject)=>{
            newuser.SyncP().catch((err)=>reject(err)).then((entity)=>resolve(entity));
        });
        
    }

    DeleteP(){
        return new Promise((resolve, reject) => {
            dbman.RemoveEntityP(User.model, this)
            .catch((err)=>{reject(err)})
            .then(()=>resolve());
        });
        
    }

    SyncP() {
        return new Promise((resolve, reject) => {
            dbman.SyncEntityP(Foo.model, this)
            .catch((err)=>reject(err))
            .then((entity)=>resolve(entity));
        });
        
    }
}

module.exports = Foo;
# PLACE YOUR PUBLIC KEYS AND CERTS HERE

### To create a self-signed certificate:

1. Generate a Private Key

>	openssl genrsa -des3 -out server.key 1024

2. Generate a CSR

>	openssl req -new -key server.key -out server.csr

3. Remove Passphrase from Key

>	cp server.key server.key.org
>	openssl rsa -in server.key.org -out server.key

4. Generate the self-signed certificate

>	openssl x509 -req -days 365 -in server.csr -signkey server.key 
-out server.crt

Once you have the .crt and .key files, you need to set up your server 
config.

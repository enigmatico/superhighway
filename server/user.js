const dbman = require('./dbman.js');
const Log = require('../utils.js').Loging;

class User {
    constructor(data){
        for(const[key, value] of Object.entries(User.model)) {
            if(key === "" || value === "" || key === "db_table_info") continue;
            this[key] = data[key];
        }
    }

    static SetModel(){
        return new Promise((resolve, reject)=>{
            this.model = {
                id: "bigint(20) NOT NULL AUTO_INCREMENT",
                unique_id: "int NOT NULL DEFAULT 0",
                registerdate: "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
                username: "varchar(30) NOT NULL",
                mail: "varchar(256) NOT NULL",
                password: "varchar(256) NOT NULL",
                hash: "varchar(256) NOT NULL",
                session: "varchar(50)",
                sessionstart: "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
                sessiondays: "int",
                avatar: "varchar(512)",
                banned: "tinyint NOT NULL DEFAULT 0",
                banneddate: "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
                banneddays: "int DEFAULT 0",
                deleted: "tinyint NOT NULL DEFAULT 0",
                messages: "bigint(20) NOT NULL DEFAULT 0",
                bot: "tinyint NOT NULL DEFAULT 0",
                db_table_info: {
                    name: "shw_users",
                    charset: "utf8mb4",
                    primary: "id",
                    unique: "unique_id"
                }
            };
            this.numfields = this.model.length;
            try{
                dbman.SyncModelP(this.model).catch((err)=>{throw err}).then((res) => resolve());
            }catch(Error){
                reject(Error);
            }
        });
    }

    static Get(id, session=false, callback){
        let idfield = '';
        if(typeof(id) == 'number'){
            idfield = 'id';
        }else if(typeof(id) == 'string'){
            if(session===false)
                idfield = 'mail';
            else
                idfield = 'session';
        }
        dbman.GetEntity(this.model, id, function(err,res){
            if(err) throw err;
            callback(new User(res[0]));
        }, idfield);
    }

    static GetP(id, session=false){
        let idfield = '';
        if(typeof(id) == 'number'){
            idfield = 'id';
        }else if(typeof(id) == 'string'){
            if(session===false)
                idfield = 'mail';
            else
                idfield = 'session';
        }
        console.log("id: ", id);
        console.log("field, ", idfield);
        return new Promise((resolve,reject) => {
            dbman.GetEntityP(this.model, id, idfield)
            .catch((err)=>reject(err))
            .then((res)=>{
                resolve(new User(res[0]));
            });
        });
    }

    static Create(data, callback){
        let mdata = {};
        let c = 0;
        for(const[key, value] of Object.entries(this.model))
        {
            if(c > data.length){
                mdata[key] = null;
            }else{
                mdata[key] = data[c];
            }
            c++;
        }
        let newuser = new User(mdata);
        newuser.SyncUser(callback);
    }

    static CreateP(data, callback){
        let mdata = {};
        let c = 0;
        for(const[key, value] of Object.entries(this.model))
        {
            if(c > data.length){
                mdata[key] = null;
            }else{
                mdata[key] = data[c];
            }
            c++;
        }
        let newuser = new User(mdata);
        return new Promise((resolve, reject)=>{
            newuser.SyncP().catch((err)=>reject(err)).then((entity)=>resolve(entity));
        });
        
    }

    /* UNTESTED */
    Delete(callback){
        dbman.RemoveEntity(User.model, this, callback);
    }

    /* UNTESTED */
    DeleteP(){
        return new Promise((resolve, reject) => {
            dbman.RemoveEntityP(User.model, this)
            .catch((err)=>{reject(err)})
            .then(()=>resolve());
        });
        
    }

    SyncUser(callback) {
        dbman.SyncEntity(User.model, this, callback);
    }

    SyncP() {
        return new Promise((resolve, reject) => {
            dbman.SyncEntityP(User.model, this)
            .catch((err)=>reject(err))
            .then((entity)=>resolve(entity));
        });
        
    }

    getInfo(){
        return {
            id: this.id,
            unique_id: this.unique_id,
            instance: "reserved",
            registerdate: this.registerdate,
            username: this.username,
            mail: this.mail,
            session: this.session,
            sessionstart: this.sessionstart,
            sessiondays: this.sessiondays,
            avatar: this.avatar,
            avatar_static: "reserved",
            friends: "reserved",
            banned: this.banned,
            banneddate: this.banneddate,
            banneddays: this.banneddays,
            deleted: this.deleted,
            messages: this.messages,
            bot: this.bot
        }
    }

    getPublicInfo(){
        return {
            unique_id: this.unique_id,
            instance: "reserved",
            registerdate: this.registerdate,
            username: this.username,
            avatar: this.avatar,
            avatar_static: "reserved",
            friends: "reserved",
            banned: this.banned,
            deleted: this.deleted,
            messages: this.messages,
            bot: this.bot
        }
    }

    increaseMessageCount(){
        this.messages++;
        this.SyncP().catch((err)=>{Log.Error(err)});
    }

    increaseMessageCountP(){
        this.messages++;
        return new Promise((resolve, reject)=>{
            this.SyncP().then(()=>{resolve()}).catch((err)=>{reject(err)});
        });
    }

    async increaseMessageCountA(){
        this.messages++;
        await this.SyncP();
    }
}

module.exports = User;